package BoilerHooks;

import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.PageFactory;

import Base.BasePage;

import javax.inject.Inject;

import GenericLibrary.CommonMethods;
import GenericLibrary.genericLibrary;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.remote.MobileCapabilityType;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;

public class hooks {
	
	private static AppiumDriver<MobileElement> driver;
	
	public static DesiredCapabilities cap = null;
	
	@Before
	public void launchApp() throws Exception{
		System.out.println("Inside before tag");
		DesiredCapabilities cap = new DesiredCapabilities();
	
		cap.setCapability(MobileCapabilityType.PLATFORM_NAME, genericLibrary.getConfigValue("mobilePlatformName"));
		cap.setCapability(MobileCapabilityType.PLATFORM_VERSION, genericLibrary.getConfigValue("mobilePlatformVersion"));
		cap.setCapability(MobileCapabilityType.AUTOMATION_NAME, genericLibrary.getConfigValue("mobileAutomationName"));
		cap.setCapability(MobileCapabilityType.DEVICE_NAME, genericLibrary.getConfigValue("mobileDevice"));
		cap.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, 60);
		cap.setCapability(MobileCapabilityType.APP, genericLibrary.getConfigValue("appPath"));
		cap.setCapability("appPackage",genericLibrary.getConfigValue("appPackage"));
		cap.setCapability("appActivity",genericLibrary.getConfigValue("appActivity"));
	
		URL url = new URL("http://0.0.0.0:4724/wd/hub");
		driver = new AndroidDriver<MobileElement>(url, cap);
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	}
	

	@After
	public void closeApp(){
		System.out.println("Done!!, Close app");
		driver.closeApp();
		}
	
	public static AppiumDriver<MobileElement> getDriver(){
		return driver;
	}
	
}
