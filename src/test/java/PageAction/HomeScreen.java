package PageAction;

import com.google.inject.Inject;

import BoilerHooks.hooks;
import GenericLibrary.CommonMethods;
import Pages.homePage;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.cucumber.core.logging.Logger;
import io.cucumber.core.logging.LoggerFactory;

public class HomeScreen extends homePage {
	
	private AppiumDriver<MobileElement> driver;
	public HomeScreen() {
		this.driver = hooks.getDriver();
	}

	public void checkElement() throws Exception{
		System.out.println("Inside checkElement method");
		homePageImage.isDisplayed();
		System.out.println("Image present in homepage");
	}

	
}
