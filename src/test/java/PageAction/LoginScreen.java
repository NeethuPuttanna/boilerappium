package PageAction;

import com.google.inject.Inject;

import Base.BasePage;
import BoilerHooks.hooks;
import Pages.loginPage;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class LoginScreen extends loginPage{
	
	private AppiumDriver<MobileElement> driver;
	public LoginScreen() {
		this.driver=hooks.getDriver();
	}
	
	public void tapOnLoginTab() {
			loginTab.click();
			signUp.isDisplayed();
			System.out.println("Image displayed in homescreen"); 
	}
	
	public void loginWithCred(String emailID, String Password) {
		System.out.println("Inside login");
		email.sendKeys(emailID);
		password.sendKeys(Password);
		signUpButton.click();
		textSuccess.isDisplayed();
		okButton.click();
		System.out.println("Login Successful!!!");
	}
	
}
