package PageAction;

import java.time.Duration;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;

import BoilerHooks.hooks;
import Pages.swipePage;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.offset.PointOption;

public class SwipeScreen extends swipePage{
	
	private AppiumDriver<MobileElement> driver;
	public SwipeScreen() {
		this.driver=hooks.getDriver();
	}
	
	Duration RIGHT;
	
	public void clickOnSwipe(){
		System.out.println("<<<<Inside clickOnSwipe method>>>>");
		swipeTab.click();
	}
	
	public void swipeCarousel() {
		System.out.println("<<<Inside swipeCarousel method>>>");
		swipeScreenWithLogs();
	}
		
	public void swipeScreenWithLogs() {
	    System.out.println("swipeScreen"); // always log your actions

	    final int ANIMATION_TIME = 200; // ms

	    final int PRESS_TIME = 200; // ms

	    int edgeBorder = 10; // better avoid edges
	    Point pointStart, pointEnd;
	    PointOption pointOptionStart, pointOptionEnd;

	    // init screen variables
	    Dimension dims = driver.manage().window().getSize();

	    // init start point = center of screen
	    pointStart = new Point(dims.width / 2, dims.height / 2);

	
	    // center of right side
	     pointEnd = new Point(dims.width - edgeBorder, dims.height / 2);
	           
	    // execute swipe using TouchAction
	    pointOptionStart = PointOption.point(pointStart.x, pointStart.y);
	    pointOptionEnd = PointOption.point(pointEnd.x, pointEnd.y);
	    System.out.println("swipeScreen(): pointStart: {" + pointStart.x + "," + pointStart.y + "}");
	    System.out.println("swipeScreen(): pointEnd: {" + pointEnd.x + "," + pointEnd.y + "}");
	    System.out.println("swipeScreen(): screenSize: {" + dims.width + "," + dims.height + "}");
	    try {
	        new TouchAction(driver)
	                .press(pointOptionStart)
	                // a bit more reliable when we add small wait
	                .waitAction()
	                .moveTo(pointOptionEnd)
	                .release().perform();
	    } catch (Exception e) {
	        System.err.println("swipeScreen(): TouchAction FAILED\n" + e.getMessage());
	        return;
	    }

	    // always allow swipe action to complete
	    try {
	        Thread.sleep(ANIMATION_TIME);
	    } catch (InterruptedException e) {
	        // ignore
	    }
	}
	
	
}
