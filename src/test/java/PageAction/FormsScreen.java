package PageAction;

import BoilerHooks.hooks;
import Pages.formsPage;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class FormsScreen extends formsPage{
	
	private AppiumDriver<MobileElement> driver;
	public FormsScreen() {
		this.driver=hooks.getDriver();
	}
	
	public void clickOnForms(){
		System.out.println("<<<<Inside toValidateFormsScreen method>>>>");
		formTab.click();
	}
	
	public void validateFormsScreen(String text) {
		System.out.println("<<<<Inside fillForm method>>>>");
		textInputField.isDisplayed();
		enterTextInInputField.sendKeys(text);
		textYouHaveTyped.isDisplayed();
		System.out.println("Result text = "+resultTextEntered.getText());
		
		if(text.equalsIgnoreCase(resultTextEntered.getText())){
			System.out.println("Text entered in Input field matches in You typed");
		} else {
			System.out.println("Text entered in Input field do not match in You typed");
		}
		
		changeSwitch.isDisplayed();
		switchText.getText().contains("ON");
		System.out.println("Default switch is ON");

		changeSwitch.click();
		switchText.getText().contains("OFF");
		System.out.println("After tap, switch is OFF");
		
		selectValueFromDropdown.click();
		dropdownOption3.click();
		System.out.println("Dropdown option selected ");
		
		buttonActive.click();
		System.out.println("Clicked on Active button");
		
		buttonPopupText.isDisplayed();
		buttonPopupTextOK.click();
		buttonInactive.isDisplayed();
	}

}
