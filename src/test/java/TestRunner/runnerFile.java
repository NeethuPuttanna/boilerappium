package TestRunner;

	import org.junit.runner.RunWith;
	import io.cucumber.junit.Cucumber;
	import io.cucumber.junit.CucumberOptions;

	@RunWith(Cucumber.class)
	@CucumberOptions(
			features = "D:\\Eclipse Installation\\BoilerAppium\\src\\test\\resource\\BoilerFeature\\EndToEndFeature.feature",
			glue= {"/StepDefinition"},
			monochrome=true,
			tags = "@login",
			plugin = {"pretty","html:target/HtmlReports"}
			)

	public class runnerFile {

	}



