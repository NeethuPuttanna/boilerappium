package Pages;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import BoilerHooks.hooks;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class swipePage {
	
	private static AppiumDriver<MobileElement> driver;
	public swipePage() {
		this.driver=hooks.getDriver();
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	@FindBy(xpath="//android.widget.TextView[@text='Swipe']")
	public static AndroidElement swipeTab;
	
	@FindBy(xpath="//android.widget.TextView[@text='Swipe horizontal']")
	public static AndroidElement swipeTitle;
	
	@FindBy(xpath="(//android.view.ViewGroup[@content-desc=\"card\"])[1]")
	public static AndroidElement card1;
	
	@FindBy(xpath="(//android.view.ViewGroup[@content-desc=\"card\"])[1]/android.widget.TextView[1]")
	public static AndroidElement card1Image;
	
	@FindBy(xpath="//android.widget.TextView[@text='FULLY OPEN SOURCE']")
	public static AndroidElement card1Text1;
	
	@FindBy(xpath="//android.widget.TextView[@text='WebdriverIO is fully open source and can be found on GitHub']")
	public static AndroidElement card1Text2;
	
	
	@FindBy(xpath="(//android.view.ViewGroup[@content-desc=\"card\"])[2]")
	public static AndroidElement card2;
	
	@FindBy(xpath="(//android.view.ViewGroup[@content-desc=\"card\"])[2]/android.widget.TextView[1]")
	public static AndroidElement card2Image;
	
	@FindBy(xpath="//android.widget.TextView[@text='CREAT COMMUNITY']")
	public static AndroidElement card2Text1;
	
	@FindBy(xpath="//android.widget.TextView[@text='WebdriverIO has a great community that supports all members.']")
	public static AndroidElement card2Text2;
	
	
	
	

}
