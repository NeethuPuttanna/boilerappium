package Pages;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import BoilerHooks.hooks;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class loginPage {
	
	private static AppiumDriver<MobileElement> driver;
	public loginPage() {
		this.driver=hooks.getDriver();
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	
	@FindBy(xpath="//android.view.ViewGroup[@content-desc=\"button-sign-up-container\"]/android.view.ViewGroup/android.widget.TextView")
	public static AndroidElement signUp;
	
	@FindBy(xpath="	//android.view.ViewGroup[@content-desc=\"Login\"]")
	public static AndroidElement loginTab;
	
	@FindBy(xpath="//android.widget.EditText[@content-desc=\"input-email\"]")
	public static AndroidElement email;
	
	@FindBy(xpath="//android.widget.EditText[@content-desc=\"input-password\"]")
	public static AndroidElement password;
	
	@FindBy(id="input-repeat-password")
	public static AndroidElement confirmPassword;

	
	@FindBy(xpath="//android.view.ViewGroup[@content-desc=\"button-LOGIN\"]")
	public static AndroidElement signUpButton;
	
//	error popup
	
	@FindBy(id="alertTitle")
	public static AndroidElement popupTextFailure;
	
	@FindBy(id="message")
	public static AndroidElement popupMessage;
	
	@FindBy(id="TRY AGAIN")
	public static AndroidElement button1;
	
//	Success popup
	
	@FindBy(xpath="//android.widget.TextView")
	public static AndroidElement textSuccess;
	
	@FindBy(xpath="//android.widget.Button")
	public static AndroidElement okButton;

	

}
