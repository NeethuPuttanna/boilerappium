package Pages;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.google.inject.Inject;

import Base.BasePage;
import BoilerHooks.hooks;
import GenericLibrary.CommonMethods;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;


public class homePage{
	
	private static AppiumDriver<MobileElement> driver;
	public homePage() {
		driver=hooks.getDriver();
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	
	@FindBy(xpath = "//android.widget.ImageView")
	public static AndroidElement homePageImage;

	@FindBy(xpath = "//android.widget.TextView[@text='WEBDRIVER']")
	public static AndroidElement textWebDriver;
	
	@FindBy(xpath = "//android.widget.TextView[@text='Home']")
	public static AndroidElement homeMenu;
	
	@FindBy(xpath = "//android.widget.TextView[@text='WebView']")
	public static AndroidElement webViewMenu;

	@FindBy(id = "Login")
	public static AndroidElement loginMenu;
	
	@FindBy(xpath = "//android.widget.TextView[@text='Forms']")
	public static AndroidElement formsMenu;
	
	@FindBy(xpath = "//android.widget.TextView[@text='Swipe']")
	public static AndroidElement swipeMenu;
	
	
}
