package Pages;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import BoilerHooks.hooks;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
//import genericLibrary.genericLibrary;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class formsPage {
	
	private static AppiumDriver<MobileElement> driver;
	public formsPage() {
		this.driver=hooks.getDriver();
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	@FindBy(xpath="//android.view.ViewGroup[@content-desc='Forms']/android.view.ViewGroup")
	public static AndroidElement formTab;
	
	
	@FindBy(xpath = "//android.widget.TextView[@text='Input field']")
	public static AndroidElement textInputField;
	
	@FindBy(xpath = "//android.widget.EditText[@content-desc=\"text-input\"]")
	public static AndroidElement enterTextInInputField;
	
	@FindBy(xpath = "//android.widget.TextView[@text='You have typed:']")
	public static AndroidElement textYouHaveTyped;
	
	@FindBy(xpath = "//android.widget.TextView[@content-desc=\"input-text-result\"]")
	public static AndroidElement resultTextEntered;
	
	@FindBy(xpath = "//android.widget.Switch[@content-desc=\"switch\"]")
	public static AndroidElement changeSwitch;
	
	@FindBy(xpath = "//android.widget.TextView[@content-desc=\"switch-text\"]")
	public static AndroidElement switchText;
	
	//Click to turn the switch ON, Click to turn the switch OFF
	
	@FindBy(xpath = "//android.widget.TextView[@text='Select a value here']")
	public static AndroidElement selectValueFromDropdown;
	
	@FindBy(xpath = "//android.widget.CheckedTextView[@text='webdriver.io is awesome']")
	public static AndroidElement dropdownOption1;
	
	@FindBy(xpath = "//android.widget.CheckedTextView[@text='Appium is awesome']")
	public static AndroidElement dropdownOption2;
	
	@FindBy(xpath = "//android.widget.CheckedTextView[@text='This app is awesome']")
	public static AndroidElement dropdownOption3;
	
	@FindBy(xpath = "//android.widget.TextView[@text='Active']")
	public static AndroidElement buttonActive;
	
	@FindBy(xpath = "//android.widget.TextView[@text='Inactive']")
	public static AndroidElement buttonInactive;
	
	@FindBy(xpath = "//android.widget.TextView[@text='This button is']")
	public static AndroidElement buttonPopupText;
	
	@FindBy(xpath = "//android.widget.Button[@text='OK']")
	public static AndroidElement buttonPopupTextOK;

}
