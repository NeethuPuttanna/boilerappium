package StepDefinition;

import BoilerHooks.hooks;
import PageAction.FormsScreen;
import PageAction.HomeScreen;
import PageAction.LoginScreen;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.cucumber.java.Scenario;
import io.cucumber.java.en.*;

public class forms {

	private static AppiumDriver<MobileElement> driver;
	public forms() {
		this.driver=hooks.getDriver();
	}
	
	HomeScreen homeScreen=new HomeScreen();
	LoginScreen loginScreen=new LoginScreen();
	FormsScreen formsScreen=new FormsScreen();
	
	@When("^I navigate to forms screen$")
	public void toclickOnForms() throws Exception{
		formsScreen.clickOnForms();
	}
	
	@Then("^I will validate Form screen by entering (.*) and other fields$")
	public void toValidateFormsScreen(String text) throws Exception {
		formsScreen.validateFormsScreen(text);
	}

}
