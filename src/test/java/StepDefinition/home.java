package StepDefinition;



import com.google.inject.Inject;
import BoilerHooks.hooks;
import PageAction.HomeScreen;
import PageAction.LoginScreen;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.cucumber.java.en.*;

public class home{
	
	private static AppiumDriver<MobileElement> driver;
	
	public home() {
		this.driver=hooks.getDriver();
	}
	
	HomeScreen homeScreen=new HomeScreen();
	LoginScreen loginScreen=new LoginScreen();
	
	
	@Given("I am on home screen")
	public void onHomeScreen() throws Throwable{
		System.out.println("Check for the presence of image in homescreen");
		homeScreen.checkElement();
	}
	
	


}
