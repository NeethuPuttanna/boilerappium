package StepDefinition;


import BoilerHooks.hooks;
import PageAction.HomeScreen;
import PageAction.LoginScreen;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.cucumber.java.Scenario;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class login {
	
  private static AppiumDriver<MobileElement> driver;
	public login() {
		this.driver=hooks.getDriver();
	}
	
	HomeScreen homeScreen=new HomeScreen();
	LoginScreen loginScreen=new LoginScreen();


	@When("^I navigate to Login screen$")
	public void i_navigate_to_login_screen() throws Throwable{
		loginScreen.tapOnLoginTab();
		System.out.println("Text Login/Signup is displayed");
	}

	@Then("^I will log in with credentials (.*) and (.*)$")
	public void i_should_be_able_to_create_new_login_credentials(String email, String password) {
		loginScreen.loginWithCred(email, password);
	}

}