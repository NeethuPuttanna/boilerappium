package StepDefinition;

import BoilerHooks.hooks;
import PageAction.SwipeScreen;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.cucumber.java.en.*;

public class swipe {
	
	SwipeScreen swipeScreen = new SwipeScreen();
	
	private static AppiumDriver<MobileElement> driver;
	public swipe() {
		this.driver=hooks.getDriver();
	}
	
	@When("^I navigate to Swipe screen$")
	public void navigateToSwipescreen() throws Throwable{
		swipeScreen.clickOnSwipe();
		System.out.println("In Swipe screen");
	}
	
	@Then("^I will swipe the carousels$")
	public void swipeCarousel() throws Throwable{
		swipeScreen.swipeCarousel();
	}

}
