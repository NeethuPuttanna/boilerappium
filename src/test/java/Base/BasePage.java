package Base;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.PageFactory;

import com.google.inject.Inject;
import com.google.inject.Injector;
import BoilerHooks.hooks;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.remote.MobileCapabilityType;
import io.cucumber.java.After;
import io.cucumber.java.Before;

public class BasePage {
	
	private AppiumDriver<MobileElement> driver;
	
//	@Inject
//	public static AppiumDriver driver;
	
	@Inject
	public hooks hook;
	
	public BasePage(AppiumDriver driver) {
		this.driver=hook.getDriver();
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
}
