
Feature: End to end scenario for Boiler 

@smoke @login
Scenario Outline: To validate login screen
Given I am on home screen
When I navigate to Login screen
Then I will log in with credentials <email> and <password>

Examples:
	|email						|password			|
	|test@test.com		| testing123	|
	
@smoke @forms
Scenario Outline: To validate form screen
Given I am on home screen
When I navigate to forms screen
Then I will validate Form screen by entering <inputText> and other fields

Examples:
	|inputText			|
	|FormTextInput	| 
	
	
@smoke @swipe
Scenario: To validate swipe screen
Given I am on home screen
When I navigate to Swipe screen
Then I will swipe the carousels 
